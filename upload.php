<?php 
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	$date = getdate();
	$dst_directory = ($date["year"] . "-" . $date["mon"] . "-" . $date["mday"] . "-" . $date["hours"] . "-" . $date["minutes"] . "-" . $date["seconds"]);
	if ($_FILES["file"]["error"] > 0)
	{
		echo "Error: " . $_FILES["file"]["error"] . "<br>";
	} else
	{
		$upload_succeeded = move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $_FILES["file"]["name"]);
		if ($upload_succeeded)
		{
			$src_image = "/var/www/upload/" . $_FILES["file"]["name"];
			$python_command = ("./generatexml/generatexml " . $src_image . " /var/www/results/" . $dst_directory . "/");
			$filename = $dst_directory . ".zip";
			$filepath = "results/";
			$zip_message = ("zip -r results/" . $filename . " results/" . $dst_directory);

			$message = system($python_command, $script_success);
			if ($script_success)
			{
				system($zip_message, $retval);
				//echo "<a href='".$filepath.$filename."'>Click for your zip bro</a>";
				header("Content-Description: File Transfer");
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".basename($filepath.$filename));
				header("Content-Transfer-Encoding: binary");
				header("Expires: 0");
				header("Pragma: public");
				header("Content-Length: ".filesize($filepath.$filename));
				ob_clean();
				flush();
				readfile($filepath.$filename);
			}
		} else
		{
			echo "Failed";
		}
	}
?>