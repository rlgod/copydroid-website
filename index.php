<!DOCTYPE html>
<html>
	<head>
		<title>CopyDroid</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<header class="centre">
		<h1 style="text-align: center">CopyDroid</h1>
	</header>
	<body>
		
		<div class="form-wrapper screenshot-form">
			<div class="form-wrapper-inner">
				<h3 class="centre">Screenshot Copier</h3>
				<p class="centre">Supply us with a screenshot of the app you'd like to copy and we will give you the 'res' directory for your Android app.</p>
				<form action="upload.php" method="post" enctype="multipart/form-data">
					<label for="file">Screenshot Image:</label>
					<input type="file" name="file" id="file"><br>
					<input type="submit" name="submit" value="Submit">
				</form>
			</div>
		</div>
	</body>
</html>